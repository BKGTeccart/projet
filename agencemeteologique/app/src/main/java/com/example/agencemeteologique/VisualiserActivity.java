package com.example.agencemeteologique;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.example.agencemeteologique.Constants.IP_ADRESS;

public class VisualiserActivity extends AppCompatActivity {

    ListView lst_data;
    ArrayList<String> dates_data;
    ArrayAdapter<String> adapter;

    class select_data extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public select_data(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = IP_ADRESS + "alldates.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                /*String msg = URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8");*/

                //bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                String[] sp = sbuff.toString().split(",");
                //fill arraylist
                for(int i=0; i<sp.length; i++){
                    dates_data.add(sp[i]);
                }
/*
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (dates_data != null) {

                            adapter = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_dropdown_item, dates_data);
                            sp_datesdata.setAdapter(adapter);

                        }
                    }
                });
*/


                //Enlever le vide en bas -> quand on fait le split la derniere virgule est considérée comme un nouveau record vide
                dates_data.remove(dates_data.size()-1);
                adapter = new ArrayAdapter<String>(c, android.R.layout.simple_list_item_1, dates_data);
                lst_data.setAdapter(adapter);


                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualiser);


        dates_data = new ArrayList<>();

        lst_data = findViewById(R.id.lst_data);

        select_data req = new select_data(this);

        req.execute(0);


    }
}