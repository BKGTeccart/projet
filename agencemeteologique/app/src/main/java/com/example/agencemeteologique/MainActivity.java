package com.example.agencemeteologique;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import static com.example.agencemeteologique.Constants.IP_ADRESS;

public class MainActivity extends AppCompatActivity {

    EditText edt_username;
    EditText edt_password;
    public String bufContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_username = findViewById(R.id.edtusername);
        edt_password = findViewById(R.id.edtpassword);
    }

    class login_phpdb extends AsyncTask {

        private Context c;
        private AlertDialog ad;

        public login_phpdb(Context c) {
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Login status");
        }


        @Override
        protected Object doInBackground(Object[] param) {
            String cible = IP_ADRESS + "login_.php";
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("Username", "utf-8") + "=" +
                        URLEncoder.encode((String) param[0], "utf-8") + "&" +
                        URLEncoder.encode("Password", "utf-8") + "=" +
                        URLEncoder.encode((String) param[1], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    sbuff.append(line + "\n");
                }

                //Log.d("result", sbuff.toString());

                bufContent = sbuff.toString();

                if(bufContent.equals("")){
                    Toast.makeText(MainActivity.this, "LOGIN INVALIDE", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, ChoixOperationActivity.class);
                    startActivity(intent);
                }

                return sbuff.toString();

            } catch (Exception ex) {
                return ex.getMessage();
            }
        }
    }


    public void btn_login(View view){
        login_phpdb req = new login_phpdb(this);

        if(edt_username.getText().toString().equals("") || edt_password.getText().toString().equals("")){
            Toast.makeText(MainActivity.this, "Champs Vide", Toast.LENGTH_LONG).show();
        } else {
            req.execute(edt_username.getText().toString(), edt_password.getText().toString());
        }

    }

}