package com.example.agencemeteologique;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ChoixOperationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_operation);
    }


    public void btnEntrer(View view){

        Intent intent = new Intent(this, InsertDataActivity.class);
        startActivity(intent);
    }

    public void btnVisualiser(View view){

        Intent intent = new Intent(this, VisualiserActivity.class);
        startActivity(intent);
    }


}