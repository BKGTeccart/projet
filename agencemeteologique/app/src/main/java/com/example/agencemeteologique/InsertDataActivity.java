package com.example.agencemeteologique;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.example.agencemeteologique.Constants.IP_ADRESS;

public class InsertDataActivity extends AppCompatActivity {

    TextView txt_lat;
    TextView txt_lng;
    TextView txt_datetime;
    String bufContent = "";
    EditText edt_temp;
    EditText edt_press;
    EditText edt_hum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data);

        txt_lat = findViewById(R.id.txt_lat);
        txt_lng = findViewById(R.id.txt_lng);
        txt_datetime = findViewById(R.id.txt_datetime);


        edt_temp = findViewById(R.id.edt_temperature);
        edt_press = findViewById(R.id.edt_pression);
        edt_hum = findViewById(R.id.edt_humidity);

        checkPermission();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        txt_datetime.setText(sdf.format(new Date()));

    }


    class insertdata_phpdb extends AsyncTask {

        private Context c;
        private AlertDialog ad;

        public insertdata_phpdb(Context c) {
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Login status");
        }


        @Override
        protected Object doInBackground(Object[] param) {
            String cible = IP_ADRESS + "insertdata.php";
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("Temperature", "utf-8") + "=" +
                        URLEncoder.encode((String) param[0], "utf-8") + "&" +
                        URLEncoder.encode("Pression", "utf-8") + "=" +
                        URLEncoder.encode((String) param[1], "utf-8") + "&" +
                        URLEncoder.encode("Humidity", "utf-8") + "=" +
                        URLEncoder.encode((String) param[2], "utf-8") + "&" +
                        URLEncoder.encode("CreatedAt", "utf-8") + "=" +
                        URLEncoder.encode((String) param[3], "utf-8")+ "&" +
                        URLEncoder.encode("GPSLat", "utf-8") + "=" +
                        URLEncoder.encode((String) param[4], "utf-8")+ "&" +
                        URLEncoder.encode("GPSLng", "utf-8") + "=" +
                        URLEncoder.encode((String) param[5], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    sbuff.append(line + "\n");
                }

                bufContent = sbuff.toString();

                if(bufContent.equals("")){
                    new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message message) {
                            // This is where you do your work in the UI thread.
                            // Your worker tells you in the message what to do.

                            Toast.makeText(c, "DONNÉES AJOUTÉES", Toast.LENGTH_LONG).show();
                        }
                    };

                } else {
                    new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message message) {
                            Toast.makeText(c, "INVALIDE", Toast.LENGTH_LONG).show();
                        }
                    };
                }

                return sbuff.toString();

            } catch (Exception ex) {
                return ex.getMessage();
            }
        }
    }



    public void btn_insertdata(View view){

        insertdata_phpdb req = new insertdata_phpdb(this);

        if(edt_temp.getText().toString().equals("") || edt_press.getText().toString().equals("") || edt_hum.getText().toString().equals("")){
            Toast.makeText(this, "Entrées manquantes", Toast.LENGTH_LONG).show();
        } else {
            req.execute(edt_temp.getText().toString(), edt_press.getText().toString(), edt_hum.getText().toString(), txt_datetime.getText().toString(), txt_lat.getText().toString(), txt_lng.getText().toString());
            finish();
        }

    }


    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {

                clsLocalisation.requestSingleUpdate(this, new clsLocalisation.LocationCallBack() {
                    @Override
                    public void onNewLocationAvailable(clsLocalisation.GPSCoordinates location) {
                        txt_lat.setText(String.valueOf(location.latitude));
                        txt_lng.setText(String.valueOf(location.longitude));

                    }
                });

            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

        } else {
            //checkPermission();
            Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_LONG).show();
        }
    }

}