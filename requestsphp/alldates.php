<?php

//connexion parameteres
$url = "mysql:host=localhost; dbname=agencemeteo";
$dbuser = "root";
$dbpw = "";


try{
    $pdo = new PDO($url, $dbuser, $dbpw);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $cmd = $pdo->prepare("select * from am_data");
    $cmd->execute();

    $out="";
    $line;
    while($line = $cmd->fetchObject()){
        $out .= "Temperature: " . "$line->Temperature" . "\n";
        $out .= "Pression: " ."$line->Pression" . "\n";
        $out .= "Humidite: " . "$line->Humidity" . "\n";
        $out .= "Date de prise: " . "$line->CreatedAt" . "\n";
        $out .= "Latitude: " . "$line->GPS_LAT" . "\n";
        $out .= "Longitude: " . "$line->GPS_LNG" . "\n,";
    }
}
catch(Exception $ex){
    //message to catch exceptions
    $out = $ex->getMessage();
}

echo $out;


?>
