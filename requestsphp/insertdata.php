<?php

//connexion parameteres
$url = "mysql:host=localhost; dbname=agencemeteo";
$dbuser = "root";
$dbpw = "";
$out="";

//user parameteres
$temp = $_POST["Temperature"];
$press = $_POST["Pression"];
$hum = $_POST["Humidity"];
$datetime_ = $_POST["CreatedAt"];
$lat = $_POST["GPSLat"];
$lng = $_POST["GPSLng"];

//script for connexion
try{
    $pdo = new PDO($url, $dbuser, $dbpw);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $cmd = $pdo->prepare("INSERT INTO am_data (Temperature, Pression, Humidity, CreatedAt, GPS_LAT, GPS_LNG) VALUES (?,?,?,?,?,?)");
    $cmd->bindParam(1, $temp);
    $cmd->bindParam(2, $press);
    $cmd->bindParam(3, $hum);
    $cmd->bindParam(4, $datetime_);
    $cmd->bindParam(5, $lat);
    $cmd->bindParam(6, $lng);

    $cmd->execute();

}
catch(Exception $ex){
    //message to catch exceptions
    $out = $ex->getMessage();
}

echo $out;


?>