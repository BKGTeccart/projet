<?php

//connexion parameteres
$url = "mysql:host=localhost; dbname=agencemeteo";
$dbuser = "root";
$dbpw = "";
$out="";

//user parameteres
$temp = "12";
$press = "22";
$hum = "44";
$datetime_ = "2020-10-26 13:00:00";
$lat = "-1.00";
$lng = "12.23";

//script for connexion
try{
    $pdo = new PDO($url, $dbuser, $dbpw);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $cmd = $pdo->prepare("INSERT INTO am_data (Temperature, Pression, Humidity, CreatedAt, GPS_LAT, GPS_LNG) VALUES (?,?,?,?,?,?)");
    $cmd->bindParam(1, $temp);
    $cmd->bindParam(2, $press);
    $cmd->bindParam(3, $hum);
    $cmd->bindParam(4, $datetime_);
    $cmd->bindParam(5, $lat);
    $cmd->bindParam(6, $lng);

    $cmd->execute();

}
catch(Exception $ex){
    //message to catch exceptions
    $out = $ex->getMessage();
}

echo $out;


?>